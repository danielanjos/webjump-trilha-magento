<?php

namespace Webjump\HelloWorld\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomObserver implements ObserverInterface
{
  public function __construct()
  {

  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    $myEventData = $observer->getData('controller_action_predispatch');
  }
}
