<?php

namespace Webjump\HelloWorld\Plugin\Framework\App\Action;

class Plugin
{
    private $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeDispatch($subject, $return)
    {
        $this->logger->debug('BeforeDispatch method');
        return null;
    }

    public function afterDispatch($subject, $return)
    {

        $this->logger->debug('AfterDisptach method');

        return $return;
    }

    public function aroundDispatch($subject, callable $proceed, ...$args)
    {
        $this->logger->debug('AroundDispatch before proceed');
        $this->logger->critical('AroundDispatch before proceed - CUSTOM CRITICAL LOGGER');
        $this->logger->info('AroundDispatch before proceed - CUSTOM INFO LOGGER');
        $this->logger->warning('WARNING LOG');

        $result = $proceed(...$args);

        $this->logger->debug('AroundDispatch after proceed');
        $this->logger->critical('AroundDispatch after proceed - CUSTOM CRITICAL LOGGER');

        return $result;
    }


}
