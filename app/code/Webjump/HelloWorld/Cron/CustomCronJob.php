<?php

namespace Webjump\HelloWorld\Cron;

use DateTimeImmutable;
use \Psr\Log\LoggerInterface;

class CustomCronJob
{

    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->info('Webjump\HelloWorld cron run at "' . (new DateTimeImmutable())->format('d/m/Y h:i:s') . '"');
    }
}
