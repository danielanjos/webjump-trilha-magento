<?php

namespace Webjump\HelloWorld\Model;

use Monolog\Logger;

class CriticalLogger extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/custom-critical.log';

    /**
     * @var int
     */
    protected $loggerType = Logger::CRITICAL;
}
