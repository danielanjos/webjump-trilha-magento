<?php

namespace Webjump\HelloWorld\Model;

use Monolog\Logger;

class DebugLogger extends \Magento\Framework\Logger\Handler\Debug
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/custom-debug.log';

    /**
     * @var int
     */
    protected $loggerType = Logger::DEBUG;
}
