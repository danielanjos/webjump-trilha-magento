<?php

namespace Webjump\HelloWorld\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CustomCommand
 */
class CustomCommand extends Command
{
    const MESSAGE = 'message';

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('webjump:custom');
        $this->setDescription('This is my first console command.');
        $this->addOption(
            self::MESSAGE,
            null,
            InputOption::VALUE_REQUIRED,
            'Message'
        );

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($message = $input->getOption(self::MESSAGE)) {
            $output->writeln('<info>Hello World! `' . $message . '`</info>');
        } else {
            $output->writeln('Hello World');
        }
    }
}
